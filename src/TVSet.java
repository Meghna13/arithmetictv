
public class TVSet {
	
	private int volume;
	private int channel;
	private boolean flag;
	
	public TVSet() {
		// TODO Auto-generated constructor stub	
		this.flag=false;
		this.volume=0;
		this.channel=0;
		
	}
	
	public boolean getFlag(){
		return this.flag;
	}
	
	public void setFlag(boolean flag){
		this.flag=flag;	
		stateTV();
	}
	
	public int getVolume(){
		return this.volume;
		
	}
	
	public void setVolume(int volume){
		if(getFlag()==true){
		this.volume=volume;
		System.out.println("Volume is set to "+this.volume);
		}else{
			System.out.println("Switch ON the TV to set the Volume");
		}
	}
	
	public int getChannel(){
		return this.channel;
	}
	
	public void setChannel(int channel){
		if(getFlag()==true){
			stateTV();
		this.channel=channel;
		System.out.println("Channel is set to "+this.channel);
		getVolume();	
		
		}else{
			System.out.println("Switch ON the TV to set the Channel");}
	}
	public void stateTV(){
		if(getFlag()==true){
			System.out.println("TV is ON");
		}else{
			System.out.println("TV is OFF");
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TVSet tvset=new TVSet();
		tvset.setFlag(true);
		tvset.setVolume(12);
		tvset.setChannel(1);
		tvset.setChannel(2);
		tvset.setChannel(3);
		tvset.setChannel(4);
		tvset.setChannel(5);
		tvset.setFlag(false);
		tvset.setVolume(12);
		tvset.setChannel(1);
	}

}
